﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ServiceModel;
using WCFCommon;
using SCADACore;

namespace AlarmDisplay
{
    class WCFClient
    {
        
        static void Main(string[] args)
        {
            Thread.Sleep(600);

            Uri address = new Uri("net.tcp://localhost:8000/IAlarmDisplay");
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            ChannelFactory<IAlarmDisplay> factory = new ChannelFactory<IAlarmDisplay>(binding, new EndpointAddress(address));

            IAlarmDisplay proxy = factory.CreateChannel();

            Console.WriteLine("-------------------------------------------------------------------------------------");
            Console.WriteLine("\t\t\t ALARM DISPLAY - WCF Client");
            Console.WriteLine("-------------------------------------------------------------------------------------");

            Console.WriteLine("Enter alarm PRIORITY you want to watch (1 or 2 or 3): ");
            Console.WriteLine("\t(1) Low");
            Console.WriteLine("\t(2) Medium");
            Console.WriteLine("\t(3) High");
            Console.WriteLine("-------------------------------------------------------------------------------------");
            /*
            int input;
            int priority;

            priority = 1;
            proxy.setPriority(priority);
            Thread thread = new Thread(new ParameterizedThreadStart(Scan));
            thread.Start(proxy);

            input = Console.Read();
            switch (input)
            {
                case 1:
                    {
                        priority = 1;
                        proxy.setPriority(priority);
                    
                        break;
                    }
                case 2:
                    {
                        priority = 2;
                        proxy.setPriority(priority);
                        
                        break;
                    }
                case 3:
                    {
                        priority = 3;
                        proxy.setPriority(priority);

                        break;
                    }
            }*/
            int priority = 1;
            proxy.setPriority(priority);

            Thread thread = new Thread(new ParameterizedThreadStart(Scan));
            thread.Start(proxy);
            while (true)
            {
                if (!Int32.TryParse(Console.ReadLine(), out priority))
                {
                    priority = 1;
                }
                proxy.setPriority(priority);
            }
        }

        public static void Scan(Object obj)
        {
            IAlarmDisplay proxy = (IAlarmDisplay)obj;
            while (true)
            {
                string alarm = proxy.alarmChecker();

                if (alarm != null)
                {
                    Console.WriteLine(alarm);
                }
            }
        }
        
    }
}
