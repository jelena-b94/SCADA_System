﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ServiceModel;
using WCFCommon;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Net;
using System.Net.Sockets;

namespace SCADACore
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
    public class WCFServer : IAlarmDisplay, IDatabaseManager, ITrending
    {
        /** Dictionary of all tags - key: Tag ID, value: Tag object */
        public static Dictionary<string, Tag> tags = new Dictionary<string, Tag>();
        /** Dictionary of all tags that are off scanning for remembering their IO Adrresses - key: Tag ID, value: IO Address */
        public static Dictionary<string, string> onOffTags = new Dictionary<string, string>();
        /** Dictionary of all active threads - key: Tag ID, value: Thread */
        public static Dictionary<string, Thread> threads = new Dictionary<string, Thread>();

        /** Write all activated alarms in text file - alarmsLog.txt */
        public static AlarmFileLogger afl = new AlarmFileLogger("alarmsLog.txt");

        /** Alarm message that is written in Alarm console. Used in method AlarmChecker() */
        public static string alarmMessage = "";
        /** Alarm priority for Alarm console. Used in method SetPriority(). Set default LOW */
        public static Priority alarmPriority = Priority.LOW;

        /** Object used for Monitor.Pulse and Monitor.Wait. Object used for object's state monitoring.
            Used in AlarmChecker() and AlarmMessage1() methods.*/
        public static string help = "1";

        /** Main thread */
        static Thread thread;

        /** Service host object */
        static ServiceHost host = null;
        /** Object of WCFServer class */
        static WCFServer model;

        /** Constructor for this class */
        public WCFServer()
        {
            Console.WriteLine("-----------------------------");
            Console.WriteLine("WCF server is running...");
            Console.WriteLine("-----------------------------");
        }

        /** Main method */
        static void Main(string[] args)
        {
            model = new WCFServer();
            Start();
            Console.ReadKey();
            Stop();
        }

        /** Start server */
        public static void Start()
        {
            /** START Deserialize */
            if ((File.Exists("database.xml")) && (new FileInfo("database.xml").Length != 0))
            {
                FileStream fs = new FileStream("database.xml", FileMode.Open);
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());

                DataContractSerializer deserializer = new DataContractSerializer(typeof(Dictionary<string, Tag>));

                tags = (Dictionary<string, Tag>)deserializer.ReadObject(reader, true);

                reader.Close();
                fs.Close();
            }
            /** END Deserialize */

            model.startTags();

            host = new ServiceHost(typeof(WCFServer));

            NetTcpBinding bindingAlarmDisplay = new NetTcpBinding();
            bindingAlarmDisplay.Security.Mode = SecurityMode.None;
            Uri addressAlarmDisplay = new Uri("net.tcp://localhost:8000/IAlarmDisplay");
            host.AddServiceEndpoint(typeof(IAlarmDisplay), bindingAlarmDisplay, addressAlarmDisplay);

            NetTcpBinding bindingDatabaseManager = new NetTcpBinding();
            bindingDatabaseManager.Security.Mode = SecurityMode.None;
            Uri addressDatabaseManager = new Uri("net.tcp://localhost:8040/IDatabaseManager");
            host.AddServiceEndpoint(typeof(IDatabaseManager), bindingDatabaseManager, addressDatabaseManager);

            NetTcpBinding bindingTrending = new NetTcpBinding();
            bindingTrending.Security.Mode = SecurityMode.None;
            Uri addressTrending = new Uri("net.tcp://localhost:8080/ITrending");
            host.AddServiceEndpoint(typeof(ITrending), bindingTrending, addressTrending);

            host.Open();

            thread = new Thread(PrintTags);
            thread.Start();
        }

        /** Stop server */
        public static void Stop()
        {
            thread.Abort();

            foreach (KeyValuePair<string, Thread> thr in threads)
                thr.Value.Abort();

            /** START Serialization */
            FileStream writter = new FileStream("database.xml", FileMode.Create);
            DataContractSerializer serializer = new DataContractSerializer(typeof(Dictionary<string, Tag>));
            serializer.WriteObject(writter, tags);
            writter.Close();
            /** END Serialization */

            afl.Close();
            host.Close();

            Console.WriteLine("-----------------------------");
            Console.WriteLine("WCF server is stopped.");
            Console.WriteLine("-----------------------------");
        }

        /** Inherited method from DATABASE MANAGER. */
        public void AddTag(Tag tag)
        {
            /*lock (tags)
            {
                if (tags.ContainsKey(tag.TagId))
                {
                    Console.WriteLine("(FAIL) Tag with id: " + tag.TagId + " is already in database.");
                    return;
                }
                if (tag is DigitalInput)
                {
                    if (((DigitalInput)tag).alarm != null)
                    {
                        ((DigitalInput)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                        ((DigitalInput)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage1);
                        ((DigitalInput)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                    }
                }
                if (tag is AnalogInput)
                {
                    ((AnalogInput)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                    ((AnalogInput)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage1);
                    ((AnalogInput)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                }
                tags.Add(tag.TagId, tag);
            }

            Console.WriteLine("(SUCCESS) Tag with id: " +tag.TagId + " is successfully added.");
            Thread thread = new Thread(new ParameterizedThreadStart(Scan));

            lock (thread) 
            {
                threads.Add(tag.TagId, thread);
            }

            thread.Start(tag);*/
            lock (tags)
            {

                if (tags.ContainsKey(tag.TagId))
                {
                    Console.WriteLine("(ERROR) Tag with ID " + tag.TagId + " already exists. Try Again.");
                    return;
                }
                if (tag is Input)
                {
                    if (((Input)tag).alarm != null)
                    {
                        ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                        ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage1);
                        ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                    }
                }
                tags.Add(tag.TagId, tag);
            }
            Console.WriteLine("(SUCCESS) Tag with ID " + tag.TagId + " is successfully added.");
            Thread thread = new Thread(new ParameterizedThreadStart(Scan));
            lock (threads)
            {
                threads.Add(tag.TagId, thread);
            }
            thread.Start(tag);
            
        }

        public void RemoveTag(string tagId)
        {
            if (tags.ContainsKey(tagId))
            {
                threads[tagId].Abort();
                lock (tags)
                {
                    tags.Remove(tagId);
                }
                lock (threads)
                {
                    threads.Remove(tagId);
                }
                Console.WriteLine("(SUCCESS) Tag with id: " + tagId + " is successfully removed.");
            }
            Console.WriteLine("(FAIL) Tag with id: " + tagId + " can't be removed.");
        }

        public void SetInitialValue(string tagId, double value)
        {            
            lock (tags)
            {
                if (tags[tagId] is DigitalOutput)               
                    ((DigitalOutput)tags[tagId]).initialValue = value;
                else if (tags[tagId] is AnalogOutput)
                    ((AnalogOutput)tags[tagId]).initialValue = value;
            }
        }

        public void StopDisplayThreads()
        {
            lock (tags)
            {
                lock (threads)
                {
                    foreach (string key in threads.Keys.ToList())
                    {
                        threads[key].Abort();
                        threads.Remove(key);
                    }

                }
            }
        }

        public void ToggleTagAutoManual(string tagId)
        {
            lock (tags)
            {
                if (tags.ContainsKey(tagId))
                {
                    if (((Input)tags[tagId]).autoManual == true)
                    {
                        ((Input)tags[tagId]).autoManual = false;
                        lock (threads)
                        {
                            //tags[tagId].Driver.Value = 0;
                            threads[tagId].Abort();
                            threads.Remove(tagId);
                        }
                    }
                    else
                    {
                        ((Input)tags[tagId]).autoManual = true;
                        Tag tag = tags[tagId];
                        lock (threads)
                        {
                            if (tag is Input)
                            {
                                if (((Input)tag).alarm != null)
                                {
                                    ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                                    ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage1);
                                    ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                                }
                            }
                            Thread thread = new Thread(new ParameterizedThreadStart(Scan));
                            lock (threads)
                            {
                                threads.Add(tag.TagId, thread);
                            }
                            thread.Start(tag);
                        }
                    }
                }
            }
            
        }

        public void ToggleTagOnOff(string tagId)
        {
            lock (tags)
            {
                if (tags.ContainsKey(tagId))
                {
                    if (((Input)tags[tagId]).onOffScan == true)
                    {
                        ((Input)tags[tagId]).onOffScan = false;
                        lock (threads)
                        {
                            
                            if (threads.ContainsKey(tagId))
                            {
                                threads[tagId].Abort();
                                threads.Remove(tagId);
                            }

                            
                            Tag tag = tags[tagId];
                            double temp = tags[tagId].Driver.Value;
                            tags[tagId].Driver.Temp = temp;
                            onOffTags.Add(tagId, tags[tagId].IOAddress);
                            tag.IOAddress = "same";
                            

                            tags[tagId] = tag;
                            if (!threads.ContainsKey(tagId))
                            {
                                Thread thread = new Thread(new ParameterizedThreadStart(Scan));
                                lock (threads)
                                {
                                    threads.Add(tagId, thread);
                                }
                                thread.Start(tag);
                            }
                        }
                    }
                    else
                    {
                        ((Input)tags[tagId]).onOffScan = true;
                        tags[tagId].Driver.Value = 0;
                        Tag tag = tags[tagId];
                        tag.IOAddress = onOffTags[tagId];
                        onOffTags.Remove(tagId);
                        lock (threads)
                        {
                            if (tag is Input)
                            {
                                if (((Input)tag).alarm != null)
                                {
                                    ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                                    ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage1);
                                    ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                                }
                            }

                            Thread thread = new Thread(new ParameterizedThreadStart(Scan));
                            lock (threads)
                            {
                                if (!threads.ContainsKey(tagId))
                                {
                                    threads.Add(tag.TagId, thread);
                                }
                            }
                            thread.Start(tag);
                        }
                    }
                }
            }
            
        }


        public List<Tag> getTags()
        {
            List<Tag> retList = new List<Tag>();
            foreach (KeyValuePair<string, Tag> tag in tags)
            {
                retList.Add(tag.Value);
            }
            return retList;
        }

        public void editTag(Tag tag)
        {
            lock (threads)
            {
                if (threads.ContainsKey(tag.TagId))
                {
                    threads[tag.TagId].Abort();
                    threads.Remove(tag.TagId);
                }

            }

            lock (tags)
            {

                tags[tag.TagId] = tag;
                if (!threads.ContainsKey(tag.TagId))
                {
                    Thread thread = new Thread(new ParameterizedThreadStart(Scan));
                    lock (threads)
                    {
                        threads.Add(tag.TagId, thread);
                    }
                    thread.Start(tag);
                }
            }
        }


        /** Inherited method from TRENDING. */
        public Dictionary<string, Tag> displayTrending()
        {
            return tags;
        }

        /** Inherited methods from ALARM DISPLAY. */
        public void setPriority(int priority)
        {
            if (priority == 2)
            {
                alarmPriority = Priority.MEDIUM;
            }
            else if (priority == 3)
            {
                alarmPriority = Priority.HIGH;
            }
            else
            {
                alarmPriority = Priority.LOW;
            }
        }

        public string alarmChecker()
        {
            
            lock (help)
            {
                if (alarmMessage.Length != 0)
                {
                   
                    Console.WriteLine("");
                    string[] tokens = alarmMessage.Split(':');
                    Priority priority = (Priority)Enum.Parse(typeof(Priority), tokens[tokens.Length - 1]);
                    if (priority >= alarmPriority)
                    {
                        string message;
                        message = alarmMessage;
                        alarmMessage = "";
                        Monitor.Pulse(help);
                        return message;
                    }
                    else
                    {
                        alarmMessage = "";
                        Monitor.Pulse(help);
                    }
                }
            }
            return null;
        }

        /** Main thread is using this method. */
        public static void PrintTags()
        {
            while (true)
            {
                foreach (KeyValuePair<string, Tag> tag in tags)
                {
                    if (tag.Value is DigitalInput)
                    {
                        if (((DigitalInput)tag.Value).onOffScan)
                            Console.WriteLine(tag.Value);
                    } else if (tag.Value is AnalogInput)
                    {
                        if (((AnalogInput)tag.Value).onOffScan)
                            Console.WriteLine(tag.Value);
                    } else
                    {
                        Console.WriteLine(tag.Value);
                    }
                }
                Thread.Sleep(3000);
            }
        }

        public void startTags()
        {
            foreach (KeyValuePair<string, Tag> entry in tags)
            {
                Tag tag = entry.Value;
                if (tag is Input)
                {
                    if (((Input)tag).alarm != null)
                    {
                        ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                        ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage1);
                        ((Input)tag).alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                    }
                }
                Thread thread = new Thread(new ParameterizedThreadStart(Scan));
                lock (threads)
                {
                    threads.Add(tag.TagId, thread);
                }
                thread.Start(tag);
            }
            /* foreach (KeyValuePair<string, Tag> tag in tags)
             {
                 Tag t = tag.Value;
                 if (t is DigitalInput)
                 {
                     if (((DigitalInput)t).Alarm != null)
                     {
                         //((DigitalInput)t).Alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                         ((DigitalInput)t).Alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage1);
                         ((DigitalInput)t).Alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                     }
                 } else if (t is AnalogInput)
                 {
                     if (((AnalogInput)t).Alarm != null)
                     {
                         //((AnalogInput)t).Alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(afl.Logger);
                         ((AnalogInput)t).Alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                         ((AnalogInput)t).Alarm.AlarmTriggered += new AlarmBlock.AlarmHendler(AlarmMessage2);
                     }
                 }
                 Thread thread = new Thread(new ParameterizedThreadStart(Scan));
                 lock (threads)
                 {
                     threads.Add(t.TagId, thread);
                 }
                 thread.Start(t);
             }*/
        }

        public static void AlarmMessage1(string message)
        {
            lock (help)
            {
                {
                    while (alarmMessage.Length != 0)
                    {
                        Monitor.Wait(help);
                    }
                    alarmMessage = message;
                }
            }
        }

        public static void AlarmMessage2(string message)
        {
            Console.WriteLine(message);
        }

        public void Scan(object tag)
        {
           
            if (tag is DigitalInput)
            {
                int milliseconds = ((DigitalInput)tag).scanTime;

                while (true)
                {

                    if (((DigitalInput)tag).autoManual)
                    {
                        double val = 0;
                        

                        double previousValue = tags[((DigitalInput)tag).TagId].Driver.Value;

                        
                        switch (((DigitalInput)tag).IOAddress)
                        {
                            case "sine":
                                val = ((DigitalInput)tag).Driver.Sine();
                                break;
                            case "cosine":
                                val = ((DigitalInput)tag).Driver.Cosine();
                                break;
                            case "ramp":
                                val = ((DigitalInput)tag).Driver.Ramp();
                                break;
                            case "triangle":
                                val = ((DigitalInput)tag).Driver.Triangle();
                                break;
                            case "rect":
                                val = ((DigitalInput)tag).Driver.Rectangle();
                                break;
                            case "digital":
                                val = ((DigitalInput)tag).Driver.Digital();
                                break;
                            case "same":
                                val = ((DigitalInput)tag).Driver.Same();
                                break;
                        }
                        lock (tags)
                        {
                            tags[((DigitalInput)tag).TagId].Driver.Value = val;
                            if (((DigitalInput)tag).alarm is DigitalAlarmBlock)
                            {
                                AlarmBlock al = ((DigitalInput)tag).alarm;
                                if (al != null)
                                    ((DigitalAlarmBlock)al).CheckValue(previousValue, val);
                            }
                            else
                            {
                                AlarmBlock al = ((DigitalInput)tag).alarm;
                                if (al != null)
                                    ((AnalogAlarmBlock)al).CheckValue(val);
                            }

                        }
                    }

                    Thread.Sleep(milliseconds);

                }
            }
            else if (tag is AnalogInput)
            {

                int milliseconds = ((AnalogInput)tag).scanTime;

                while (true)
                {

                    if (((AnalogInput)tag).autoManual == true)
                    {
                        double val = 0;

                        double previousValue = tags[((AnalogInput)tag).TagId].Driver.Value;

                        switch (((AnalogInput)tag).IOAddress)
                        {
                            case "sine":
                                val = ((AnalogInput)tag).Driver.Sine();
                                break;
                            case "cosine":
                                val = ((AnalogInput)tag).Driver.Cosine();
                                break;
                            case "ramp":
                                val = ((AnalogInput)tag).Driver.Ramp();
                                break;
                            case "triangle":
                                val = ((AnalogInput)tag).Driver.Triangle();
                                break;
                            case "rect":
                                val = ((AnalogInput)tag).Driver.Rectangle();
                                break;
                            case "digital":
                                val = ((AnalogInput)tag).Driver.Digital();
                                break;
                            case "same":
                                val = ((AnalogInput)tag).Driver.Same();
                                break;
                        }
                        lock (tags)
                        {
                            tags[((AnalogInput)tag).TagId].Driver.Value = val;
                            if (((AnalogInput)tag).alarm is DigitalAlarmBlock)
                            {
                                AlarmBlock al = ((AnalogInput)tag).alarm;
                                if (al != null)
                                    ((DigitalAlarmBlock)al).CheckValue(previousValue, val);
                            }
                            else
                            {
                                AlarmBlock al = ((AnalogInput)tag).alarm;
                                if (al != null)
                                    ((AnalogAlarmBlock)al).CheckValue(val);
                            }

                        }
                    }

                    Thread.Sleep(milliseconds);

                }
            }

        
        }

    }
}
