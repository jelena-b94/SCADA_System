﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WCFCommon;


namespace Trending
{
    public partial class TrendingForm : Form
    {
        System.Windows.Forms.Timer MyTimer = new System.Windows.Forms.Timer();

        List<System.Windows.Forms.Timer> timers = new List<System.Windows.Forms.Timer>();

        Dictionary<string, Tag> tags;
        ITrending proxy;

        List<Color> colors = new List<Color>();

        Dictionary<string, double> lastValue;

        //Dictionary<string, System.Windows.Forms.CheckBox> boxes = new Dictionary<string, CheckBox>();
        Dictionary<string, System.Windows.Forms.DataVisualization.Charting.Series> series = new Dictionary<string, Series>();
        Dictionary<string, System.Windows.Forms.DataVisualization.Charting.Legend> legends = new Dictionary<string, Legend>();
        
        public TrendingForm(ITrending p)
        {
            tags = new Dictionary<string, Tag>();

            lastValue = new Dictionary<string, double>();

            proxy = p;

            InitializeComponent();
            chart1.ChartAreas[0].AxisX.IsStartedFromZero = true;
            chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = false;
            //chart1.ChartAreas[0].AxisY.Interval = 50;
            //chart1.Series[0].XValueType = ChartValueType.Time;
            chart1.ChartAreas[0].AxisX.ScaleView.SizeType = DateTimeIntervalType.Seconds;
            chart1.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;
            chart1.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Seconds;
            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
            chart1.ChartAreas[0].AxisX.Interval = 0;
            chart1.ChartAreas[0].AxisX.Minimum = DateTime.Now.ToOADate();
            chart1.ChartAreas[0].AxisX.Maximum = DateTime.Now.AddSeconds(10).ToOADate();

            chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisY.ScrollBar.IsPositionedInside = true;
            chart1.ChartAreas[0].AxisY.ScrollBar.Enabled = true;
            chart1.ChartAreas[0].AxisY.IsLabelAutoFit = true;
            chart1.ChartAreas[0].AxisY.ScaleView.Size = 105;
            chart1.ChartAreas[0].AxisY.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll;

            colors.Add(Color.DarkRed);
            colors.Add(Color.Turquoise);
            colors.Add(Color.Coral);
            colors.Add(Color.DarkViolet);
            colors.Add(Color.MediumBlue);
            colors.Add(Color.Lime);


            tags = proxy.displayTrending();

            for (int i = 0; i < tags.Count; i++)
            {
                Tag tag = tags.ElementAt(i).Value;
                series.Add(tag.TagId, new System.Windows.Forms.DataVisualization.Charting.Series()
                {
                    XValueType = ChartValueType.Time,
                    ChartArea = "ChartArea1",
                    ChartType = SeriesChartType.Line,
                    Name = tag.TagId

                });

                this.chart1.Series.Add(series[tag.TagId]);

                chart1.Series[tag.TagId].ChartType = SeriesChartType.Line;
                chart1.Series[tag.TagId].Color = colors[i % 6];
                chart1.Series[tag.TagId].BorderWidth = 2;

                lastValue[tag.TagId] = tag.Driver.Value;

                legends.Add(tag.TagId, new System.Windows.Forms.DataVisualization.Charting.Legend()
                {
                    Name = tag.TagId
                });

                this.chart1.Legends.Add(legends[tag.TagId]);
                
            }


            System.Windows.Forms.Timer MyTimer = new System.Windows.Forms.Timer();
            MyTimer.Interval = (200);
            MyTimer.Tick += new EventHandler(MyTimer_Tick);

            timers.Add(MyTimer);
            MyTimer.Start();
        }





        private void MyTimer_Tick(object sender, EventArgs e)
        {
            tags = proxy.displayTrending();

            for (int i = 0; i < tags.Count; i++)
            {

                Tag tag = tags.ElementAt(i).Value;

                double prethodna;
                if (lastValue.ContainsKey(tag.TagId))
                {
                    series[tag.TagId].Enabled = true;
                    legends[tag.TagId].Enabled = true;
                    
                    prethodna = lastValue[tag.TagId];
                    chart1.ChartAreas[0].AxisX.Maximum = DateTime.Now.ToOADate();
                    if (tag.Driver.Value != prethodna)
                        //chart1.Series[tag.TagId].Points.Add(tags[tag.TagId].Driver.Value);
                        chart1.Series[tag.TagId].Points.AddXY(DateTime.Now.ToOADate(), tags[tag.TagId].Driver.Value);
                    else if (tag.IOAddress.Equals("rect"))
                    {
                        // chart1.Series[tag.TagId].Points.Add(tags[tag.TagId].Driver.Value);
                        chart1.Series[tag.TagId].Points.AddXY(DateTime.Now.ToOADate(), tags[tag.TagId].Driver.Value);
                    }
                    else if (tag.IOAddress.Equals("digital"))
                    {
                        //  chart1.Series[tag.TagId].Points.Add(tags[tag.TagId].Driver.Value);
                        chart1.Series[tag.TagId].Points.AddXY(DateTime.Now.ToOADate(), tags[tag.TagId].Driver.Value);
                    }

                    lastValue[tag.TagId] = tag.Driver.Value;
                }
                else
                {
                    series.Add(tag.TagId, new System.Windows.Forms.DataVisualization.Charting.Series()
                    {
                        XValueType = ChartValueType.Time,
                        ChartArea = "ChartArea1",
                        ChartType = SeriesChartType.Line,
                        Name = tag.TagId

                    });

                    this.chart1.Series.Add(series[tag.TagId]);

                    chart1.Series[tag.TagId].ChartType = SeriesChartType.Line;
                    chart1.Series[tag.TagId].Color = colors[i];
                    chart1.Series[tag.TagId].BorderWidth = 2;

                    lastValue[tag.TagId] = tag.Driver.Value;


                    legends.Add(tag.TagId, new System.Windows.Forms.DataVisualization.Charting.Legend()
                    {
                        Name = tag.TagId
                    });

                    this.chart1.Legends.Add(legends[tag.TagId]);
                    
                }

            }
        }

    }
}
