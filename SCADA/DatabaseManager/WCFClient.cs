﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ServiceModel;
using WCFCommon;


namespace DatabaseManager
{
    sealed class WCFClient
    {
     
        public static void printMenu()
        {
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("\t\t\tDATABASE MANAGER - WCF Client");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Choose one of the options: ");
            Console.WriteLine("\t(1) Add analog/digital tag to server");
            Console.WriteLine("\t(2) Remove analog/digital tag from server");
            Console.WriteLine("\t(3) Edit analog/digital tag from server");
            Console.WriteLine("\t(4) Turn ON/OFF scanning of tags for console overview");
            Console.WriteLine("\t(5) Auto/Manual scanning of tags from Simulation Driver");
            Console.WriteLine("\t(6) Set initial value of tag");
            Console.WriteLine("\t(7) List all tags from server");
            Console.WriteLine("\t(8) Stop display");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("\t(0) - Quit\n");

        }

        public static void printAllTags(IDatabaseManager proxy)
        {
            Console.WriteLine("List of all tags:");

            List<Tag> tagList = proxy.getTags();
            if (tagList.Count == 0)
                Console.WriteLine("\t(EMPTY)");
            foreach (Tag tag in tagList)
            {
                Console.WriteLine(tag);
            }
            Console.WriteLine("");
        }

        static void Main()
        {
            Thread.Sleep(1000);

            Uri address = new Uri("net.tcp://localhost:8040/IDatabaseManager");
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            ChannelFactory<IDatabaseManager> factory = new ChannelFactory<IDatabaseManager>(binding, new EndpointAddress(address));

            IDatabaseManager proxy = factory.CreateChannel();

            /* Make some values */
           /* AnalogInput analogInput = new AnalogInput("Tag No 1", "Yellow pump", "cosine", 111, 20, 70, "m", new AnalogAlarmBlock());
            proxy.AddTag(analogInput);

            AnalogInput analogInput2 = new AnalogInput("Tag No 2", "Yellow pump", "sine", 111, 40, 80, "m", new AnalogAlarmBlock());
            proxy.AddTag(analogInput2);

            AnalogInput analogInput3 = new AnalogInput("Tag No 3", "Yellow pump", "rect", 111, 40, 80, "m", new AnalogAlarmBlock());
            proxy.AddTag(analogInput3);*/

            AnalogInput analogInput4 = new AnalogInput("Tag No 4", "Yellow pump", "triangle", 100, 5, 15, 20, "m", new AnalogAlarmBlock(DateTime.Now, AlarmType.Hi, Priority.LOW, "Tag No 4",
 
            5,
            15//,

            ));
            proxy.AddTag(analogInput4);
            /*
            DigitalInput digInput = new DigitalInput("Tag No 5", "Digital input desc", "digital", 111, new DigitalAlarmBlock(), true, true);
            proxy.AddTag(digInput);
            
            DigitalOutput digitalOutput = new DigitalOutput("Tag No 6", "Digital out desc", "digital", 12);
            proxy.AddTag(digitalOutput);
            */
            //AnalogInput analogInput3 = new AnalogInput("Tag No 5", "Yellow pump", "rectangle", 111, 40, 80, "m", new AnalogAlarmBlock());
            //proxy.AddTag(analogInput3);


            int choice = -1;
            string input;
            while (choice != 0)
            {
                printMenu();
                Console.Write(">> ");
                input = Console.ReadLine();
                if (!Int32.TryParse(input, out choice))
                {
                    choice = -1;
                }
                switch (choice)
                {
                    case 0:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("Exited");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            break;
                        }
                    case 1:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("Add New Tag");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            AddTag(proxy);                          
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("Remove Tag");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            RemoveTag(proxy);
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("Edit Tag");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            EditTag(proxy);
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("Turn ON/OFF Scan");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

                            printAllTags(proxy);

                            Console.Write("Enter Tag ID >> ");
                            string tagId = Console.ReadLine();

                            proxy.ToggleTagOnOff(tagId);

                            break;
                        }
                    case 5:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("Auto/Manual Scan");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

                            printAllTags(proxy);

                            Console.Write("Enter Tag ID >> ");
                            string tagId = Console.ReadLine();

                            proxy.ToggleTagAutoManual(tagId);

                            break;
                        }
                    case 6:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("Set Initial Value of the Tag");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

                            printAllTags(proxy);

                            Console.Write("Enter Tag ID >> ");
                            string tagId = Console.ReadLine();

                            bool statement = false;
                            double value = 0;
                            while (!statement)
                            {
                                Console.Write("Enter new value >> ");
                                if (Double.TryParse(Console.ReadLine(), out value))
                                {
                                    statement = true;
                                }
                                else
                                {
                                    Console.WriteLine("(ERROR) Invalid input. Not a number.");
                                }
                            }

                            proxy.SetInitialValue(tagId, value);
                            break;
                        }
                    case 7:
                        {
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            Console.WriteLine("List all tags");
                            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

                            printAllTags(proxy);

                            break;
                        }
                    case 8:
                        {

                            proxy.StopDisplayThreads();

                            break;
                            
                        }
                    default:
                        {
                            Console.WriteLine("(ERROR) Input error, try to enter again.");
                            break;
                        }

                }
            }
        }

        public static void PrintAddMenu()
        {
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Choose one of the following:");
            Console.WriteLine("\t(1) Digital Input ~ DI");
            Console.WriteLine("\t(2) Digital Output ~ DO");
            Console.WriteLine("\t(3) Analog Input ~ AI");
            Console.WriteLine("\t(4) Analog Output ~ AO");
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("\t(0) Back\n");           
        }

        public static void AddTag(IDatabaseManager proxy)
        {
            int choice = -1;
            while (choice != 0)
            {
                PrintAddMenu();
                Console.Write(">> ");
                if (!Int32.TryParse(Console.ReadLine(), out choice))
                    choice = -1;
                switch (choice)
                {
                    case 1:
                        {
                            Console.WriteLine("---------------------------------------------------------");
                            Console.WriteLine("Set Up Digital Input");
                            Console.WriteLine("---------------------------------------------------------");
                            AddDI(proxy);
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("---------------------------------------------------------");
                            Console.WriteLine("Set Up Digital Output");
                            Console.WriteLine("---------------------------------------------------------");
                            AddDO(proxy);
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("---------------------------------------------------------");
                            Console.WriteLine("Set Up Analog Input");
                            Console.WriteLine("---------------------------------------------------------");
                            AddAI(proxy);
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("---------------------------------------------------------");
                            Console.WriteLine("Set Up Analog Output");
                            Console.WriteLine("---------------------------------------------------------");
                            AddAO(proxy);
                            break;
                        }
                    case 0:
                        {
                            Console.WriteLine("Pressed Back.");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("(ERROR) Invalid input. Try again.");
                            break;
                        }
                }
            }
        }

        public static void RemoveTag(IDatabaseManager proxy)
        { 
            printAllTags(proxy);

            Console.Write("Enter tag Id you want to remove >> ");

            proxy.RemoveTag(Console.ReadLine());

        }

        public static void EditTag(IDatabaseManager proxy)
        {
            Console.WriteLine("Edit tag...");
        }

        public static void AddDI(IDatabaseManager proxy)
        {
            /** Needed information for DIGITAL INPUT:
            *        - tagId
            *        - description
            *        - driver (already set initial)
            *        - I/O address
            *        - scanTime
            *        - alarms
            *        - on/off scan
            *        - auto/manual
            **/

            /* TAG ID - tagId */
            string tagId = "";
            bool statement1 = true;
            while (statement1)
            {
                statement1 = false;
                Console.Write("Tag ID >> ");
                tagId = Console.ReadLine().Trim();
                List<Tag> list = proxy.getTags();
                foreach (Tag tag in list)
                {
                    if (tag.TagId.Equals(tagId))
                    {
                        statement1 = true;
                        Console.WriteLine("Tag with this ID already exists.");
                        break;
                    }
                }
            }

            /* DESCRIPTION - description */
            Console.Write("Description >> ");
            string description = Console.ReadLine().Trim();

            /* I/O ADDRESS */
            string ioAddress = "digital";

            /* SCAN TIME */
            string scan;
            int scanTime = -1;
            bool statement3 = false;
            while (!statement3)
            {
                Console.Write("Scan time >> ");
                scan = Console.ReadLine().Trim();
                if (!Int32.TryParse(scan, out scanTime))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    if (scanTime > 0)
                    {
                        statement3 = true;
                    }
                    else
                    {
                        Console.WriteLine("(ERROR) Invalid input. Value is negative. Try again.");
                    }
                }
            }

            /* ALARMS */
            string alarmInput = "n";
            bool statement4 = false;
            while (!statement4)
            {
                Console.WriteLine("Do you want alarm for this tag? (y/n)");
                Console.Write(">> ");
                alarmInput = Console.ReadLine().Trim();
                if (alarmInput.Equals("y") || alarmInput.Equals("n"))
                {
                    statement4 = true;
                }
                else
                {
                    Console.WriteLine("(ERROR) Invalid input. Try again.");
                }
                
            }

            /* Setting Digital Alarm */
            DigitalAlarmBlock digAlarm = null;
            if (alarmInput.Equals("y"))
            {
                /* Alarm PRIORITY */
                string priorityInput;
                int priority;
                bool statement5 = false;
                Priority p = Priority.LOW;
                while (!statement5)
                {
                    Console.WriteLine("Choose alarm priority:");
                    Console.WriteLine("(1) LOW");
                    Console.WriteLine("(2) MEDIUM");
                    Console.WriteLine("(3) HIGH");
                    Console.Write(">> ");
                    priorityInput = Console.ReadLine().Trim();
                    if (!Int32.TryParse(priorityInput, out priority))
                    {
                        Console.WriteLine("(ERROR) Invalid input. Try again.");
                    }
                    else
                    {
                        switch (priority)
                        {
                            case 1:
                                {
                                    p = Priority.LOW;
                                    statement5 = true;
                                    break;
                                }
                            case 2:
                                {
                                    p = Priority.MEDIUM;
                                    statement5 = true;
                                    break;
                                }
                            case 3:
                                {
                                    p = Priority.HIGH;
                                    statement5 = true;
                                    break;
                                }
                            default:
                                {
                                    Console.WriteLine("(ERROR)Invalid input.Try again.");
                                    break;
                                }
                        }
                       
                    }
                }

                /* Alarm TYPE */
                string typeInput;
                int type;
                bool statement6 = false;
                AlarmType t = AlarmType.None;
                while (!statement6)
                {
                    Console.WriteLine("Choose alarm type:");
                    Console.WriteLine("(1) Open");
                    Console.WriteLine("(2) Close");
                    Console.WriteLine("(3) Change Of States");
                    Console.WriteLine("(4) None");
                    Console.Write(">> ");
                    typeInput = Console.ReadLine().Trim();
                    if (!Int32.TryParse(typeInput, out type))
                    {
                        Console.WriteLine("(ERROR)Invalid input.Try again.");
                    }
                    else
                    {
                        switch (type)
                        {
                            case 1:
                                {
                                    t = AlarmType.Open;
                                    statement6 = true;
                                    break;
                                }
                            case 2:
                                {
                                    t = AlarmType.Close;
                                    statement6 = true;
                                    break;
                                }
                            case 3:
                                {
                                    t = AlarmType.ChangeOfState;
                                    statement6 = true;
                                    break;
                                }
                            case 4:
                                {
                                    t = AlarmType.None;
                                    statement6 = true;
                                    break;
                                }
                            default:
                                {
                                    Console.WriteLine("(ERROR)Invalid input.Try again.");
                                    break;
                                }
                        }
                        
                    }
                }

                digAlarm = new DigitalAlarmBlock(tagId, p, t);

            }

            DigitalInput digInput = new DigitalInput(tagId, description, ioAddress, scanTime, digAlarm,true, true);

            proxy.AddTag(digInput);
        }

        public static void AddDO(IDatabaseManager proxy)
        {
            /** Needed information for DIGITAL OUTPUT:
            *        - tagId
            *        - description
            *        - driver (already set initial)
            *        - I/O address
            *        - initial value
            **/

            /* TAG ID - tagId */
            string tagId = "";
            bool statement1 = true;
            //int pom = 1;
            while (statement1)
            {
                statement1 = false;
                Console.Write("Tag Id >> ");
                tagId = Console.ReadLine().Trim();
                List<Tag> list = proxy.getTags();
                foreach (Tag tag in list)
                {
                    if (tag.TagId.Equals(tagId))
                    {
                        statement1 = true;
                        Console.WriteLine("(ERROR) Invalid input. Try again.");
                        break;
                    }
                }
            }

            /* DESCRIPTION - description */
            Console.Write("Description >> ");
            string description = Console.ReadLine().Trim();

            /* I/O ADDRESS */
            Console.Write("IOAdress >> ");
            string ioAddress = Console.ReadLine().Trim();

            /* INITIAL VALUE */
            string initialInput = "";
            double initial = 0;
            bool statement2 = false;
            while (!statement2)
            {
                Console.Write("Initial Value >> ");
                initialInput = Console.ReadLine().Trim();
                if (!Double.TryParse(initialInput, out initial))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    statement2 = true;
                }
            }

            DigitalOutput digitalOutput = new DigitalOutput(tagId, description, ioAddress, initial);

            proxy.AddTag(digitalOutput);
        }

        public static void AddAI(IDatabaseManager proxy)
        {
            /** Needed information for ANALOG INPUT:
            *        - tagId
            *        - description
            *        - driver (already set initial)
            *        - I/O address
            *        - scan time
            *        - alarms
            *        - on/off scan
            *        - auto/manual
            *        - low limit
            *        - high limit
            *        - units
            **/

            /* TAG ID - tagId */
            string tagId = "";
            bool statement1 = true;
            while (statement1)
            {
                statement1 = false;
                Console.Write("Tag ID >> ");
                tagId = Console.ReadLine().Trim();
                List<Tag> list = proxy.getTags();
                foreach (Tag tag in list)
                {
                    if (tag.TagId.Equals(tagId))
                    {
                        statement1 = true;
                        Console.WriteLine("(ERROR) Invalid input. Try again.");
                        break;
                    }
                }
            }

            /* DESCRIPTION - description */
            Console.Write("Description >> ");
            string description = Console.ReadLine().Trim();

            /* I/O ADDRESS */
            bool statement2 = false;
            string ioAddress = "";
            while (!statement2)
            {
                Console.Write("IOAddress (sine/cosine/ramp/triangle/rectangle) >> ");
                ioAddress = Console.ReadLine().Trim();
                if (ioAddress.Equals("sine") || ioAddress.Equals("cosine") || ioAddress.Equals("ramp") || ioAddress.Equals("triangle") || ioAddress.Equals("recttangle"))
                {
                    statement2 = true;
                }
                else
                {
                    Console.WriteLine("(ERROR) Invalid input. Try again.");
                }
            }

            /* SCAN TIME */
            bool statement3 = false;
            string scanInput = "";
            int scanTime = -1;
            while (!statement3)
            {
                Console.Write("Scan time >> ");
                scanInput = Console.ReadLine().Trim();
                if (!Int32.TryParse(scanInput, out scanTime))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    statement3 = true;
                }
            }

            /* LOW INPUT */
            string lowLimitInput = "";
            double lowLimit = -1;
            bool statement4 = false;
            while (!statement4)
            {
                Console.Write("Low limit value >> ");
                lowLimitInput = Console.ReadLine().Trim();
                if (!Double.TryParse(lowLimitInput, out lowLimit))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    statement4 = true;
                }
            }

            /* HIGH INPUT */
            string highLimitInput = "";
            double highLimit = -1;
            bool statement5 = false;
            while (!statement5)
            {
                Console.Write("High limit value >> ");
                highLimitInput = Console.ReadLine().Trim();
                if (!Double.TryParse(highLimitInput, out highLimit))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    statement5 = true;
                }
            }

            /* AMPLITUDE */
            string amplInput = "";
            double ampl = -1;
            bool statement10 = false;
            while (!statement10)
            {
                Console.Write("Amplitude >> ");
                amplInput = Console.ReadLine().Trim();
                if (!Double.TryParse(amplInput, out ampl))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    statement5 = true;
                }
            }

            /* UNITS */
            Console.Write("Units >> ");
            string units = Console.ReadLine();

            /* ALARMS */
            string alarmInput = "";
            bool statement6 = false;
            while (!statement6)
            {
                Console.WriteLine("Do you want alarm for this tag? (y/n)");
                Console.Write(">> ");
                alarmInput = Console.ReadLine().Trim();
                if (alarmInput.Equals("y") || alarmInput.Equals("n"))
                {
                    statement6 = true;
                }
                else
                {
                    Console.WriteLine("(ERROR) Invalid input. Try again.");
                }

            }

            AnalogAlarmBlock analogAlarm = null;

            if (alarmInput.Equals("y"))
            {
                /* Alarm PRIORITY */
                string priorityInput;
                int priority;
                bool statement7 = false;
                Priority p = Priority.LOW;
                while (!statement7)
                {
                    Console.WriteLine("Choose alarm priority:");
                    Console.WriteLine("(1) LOW");
                    Console.WriteLine("(2) MEDIUM");
                    Console.WriteLine("(3) HIGH");
                    Console.Write(">> ");
                    priorityInput = Console.ReadLine().Trim();
                    if (!Int32.TryParse(priorityInput, out priority))
                    {
                        Console.WriteLine("(ERROR) Invalid input. Try again.");
                    }
                    else
                    {
                        switch (priority)
                        {
                            case 1:
                                {
                                    p = Priority.LOW;
                                    statement7 = true;
                                    break;
                                }
                            case 2:
                                {
                                    p = Priority.MEDIUM;
                                    statement7 = true;
                                    break;
                                }
                            case 3:
                                {
                                    p = Priority.HIGH;
                                    statement7 = true;
                                    break;
                                }
                            default:
                                {
                                    Console.WriteLine("(ERROR)Invalid input.Try again.");
                                    break;
                                }
                        }

                    }
                }

                /* Alagrm LOW LIMIT */
                string lowInput = "";
                double low = 0;
                double lowLow = 0;
                bool statement8 = false;
                while (!statement8)
                {
                    Console.Write("Low limit >> ");
                    lowInput = Console.ReadLine().Trim();
                    if (!Double.TryParse(lowInput, out low))
                    {
                        Console.WriteLine("(ERROR)Invalid input.Try again.");
                    }
                    else
                    {
                        statement8 = true;
                        if (low < lowLimit)
                        {
                            low = lowLimit;
                            lowLow = lowLimit;
                        }
                    }
                }

                /* Alagrm LOW LIMIT */
                string highInput = "";
                double high = 0;
                //double highHigh = 0;
                bool statement9 = false;
                while (!statement9)
                {
                    Console.Write("High Limit >> ");
                    highInput = Console.ReadLine().Trim();
                    if (!Double.TryParse(highInput, out high))
                    {
                        Console.WriteLine("(ERROR)Invalid input.Try again.");
                    }
                    else
                    {
                        statement9 = true;
                        /*if (high > highLimit)
                        {
                            high = highLimit;
                            highHigh = highLimit;
                        }*/
                    }
                }

                analogAlarm = new AnalogAlarmBlock(p, tagId, 
                    //lowLow, 
                    low, high//, 
                    //highHigh
                    );
            }
            AnalogInput analogInput = new AnalogInput(tagId, description, ioAddress, scanTime, lowLimit, highLimit, ampl, units, analogAlarm);

            proxy.AddTag(analogInput);
        }

        public static void AddAO(IDatabaseManager proxy)
        {
            /** Needed information for ANALOG OUTPUT:
            *        - tagId
            *        - description
            *        - driver (already set initial)
            *        - I/O address
            *        - initial value
            *        - low limit
            *        - high limit
            *        - units
            **/

            /* TAG ID - tagId */
            string tagId = "";
            bool statement1 = true;
            while (statement1)
            {
                statement1 = false;
                Console.Write("Tag ID >> ");
                tagId = Console.ReadLine().Trim();
                List<Tag> list = proxy.getTags();
                foreach (Tag tag in list)
                {
                    if (tag.TagId.Equals(tagId))
                    {
                        statement1 = true;
                        Console.WriteLine("(ERROR) Tag already exists. Try again.");
                        break;
                    }
                }
            }

            /* DESCRIPTION */
            Console.Write("Description >> ");
            string description = Console.ReadLine().Trim();

            /* I/O ADDRESS */
            Console.Write("IOAdress >> ");
            string ioAddress = Console.ReadLine().Trim();

            /* INITIAL VALUE */
            string initialInput = "";
            double initial = 0;
            bool initialTrue = false;
            while (!initialTrue)
            {
                Console.Write("Initial value >> ");
                initialInput = Console.ReadLine().Trim();
                if (!Double.TryParse(initialInput, out initial))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    initialTrue = true;
                }
            }

            /* LOW INPUT */
            string lowLimitInput = "";
            double lowLimit = -1;
            bool statement4 = false;
            while (!statement4)
            {
                Console.Write("Low limit value >> ");
                lowLimitInput = Console.ReadLine().Trim();
                if (!Double.TryParse(lowLimitInput, out lowLimit))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    statement4 = true;
                }
            }

            /* HIGH INPUT */
            string highLimitInput = "";
            double highLimit = -1;
            bool statement5 = false;
            while (!statement5)
            {
                Console.Write("High limit value >> ");
                highLimitInput = Console.ReadLine().Trim();
                if (!Double.TryParse(highLimitInput, out highLimit))
                {
                    Console.WriteLine("(ERROR) Invalid input. Not a number. Try again.");
                }
                else
                {
                    statement5 = true;
                }
            }

            /* UNITS */
            Console.Write("Units >> ");
            string units = Console.ReadLine();


            AnalogOutput analogOutput = new AnalogOutput(tagId, description, ioAddress, initial, lowLimit, highLimit, units);

            proxy.AddTag(analogOutput);
        
        }

        public static Tag getTagById(string tagId, IDatabaseManager proxy)
        {
            List<Tag> tagList = proxy.getTags();
            if (tagList.Count == 0)
                Console.WriteLine("(ERROR) There is no tag with this ID.");
            foreach (Tag tag in tagList)
            {
                if (tag.TagId.Equals(tagId))
                    return tag;
            }
            return null;
        }
    }
}
