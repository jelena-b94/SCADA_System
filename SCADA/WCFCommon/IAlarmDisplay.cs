﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace WCFCommon
{
    [ServiceContract]
    public interface IAlarmDisplay
    {
        [OperationContract]
        string alarmChecker();

        [OperationContract]
        void setPriority(int priority);
    }
}
