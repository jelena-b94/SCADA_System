﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    
    [DataContract]
    public class AnalogOutput : Output
    {
        [DataMember]
        public double lowLimit { get; set; }

        [DataMember]
        public double highLimit { get; set; }

        [DataMember]
        public string units { get; set; }


        public AnalogOutput() { }

        public AnalogOutput(string tagName, string description, string IOaddress, double initialValue, double lowLimit, double highLimit, string units)
            : base(tagName, description, IOaddress, initialValue)
        {
            this.lowLimit = lowLimit;
            this.highLimit = highLimit;
            this.units = units;
        }

        public override string ToString()
        {
            return "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
                + " Tag ID: " + this.TagId + " Initial value: " + this.initialValue + " Description: " + this.Description + "\n I/O Address: " + this.IOAddress + " Low limit: " + this.lowLimit + " High limit: " + this.highLimit + " Units: " + this.units 
                + "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        }
    }
}
