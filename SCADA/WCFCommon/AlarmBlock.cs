﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    [KnownType(typeof(DigitalAlarmBlock))]
    [KnownType(typeof(AnalogAlarmBlock))]
    public abstract class AlarmBlock
    {
        public delegate void AlarmHendler(string message);
        public event AlarmHendler AlarmTriggered;

        [DataMember]
        private DateTime alarmTime;
        [DataMember]
        private AlarmType alarmType;
        [DataMember]
        private Priority alarmPriority;
        [DataMember]
        private string tagId;

        public DateTime AlarmTime
        {
            get { return alarmTime; }
            set { alarmTime = value; }
        }

        public AlarmType AlarmType
        {
            get { return alarmType; }
            set { alarmType = value; }
        }

        public Priority AlarmPriority
        {
            get { return alarmPriority; }
            set { alarmPriority = value; }
        }

        public string TagId
        {
            get { return tagId; }
            set { tagId = value; }
        }

        public AlarmBlock()
        {
            /*this.alarmTime = new DateTime();
            this.alarmType = AlarmType.None;
            this.tagId = "";*/
        }
        /*
        public AlarmBlock(DateTime alarmTime, AlarmType alarmType, Priority alarmPriority, string tagId)
        {
            this.alarmTime = alarmTime;
            this.alarmType = alarmType;
            this.alarmPriority = alarmPriority;
            this.tagId = tagId;
        }
        */
        public AlarmBlock(Priority alarmPriority, string tagId)
        {
            this.tagId = tagId;
            this.alarmPriority = alarmPriority;
        }

        public override string ToString()
        {
            string retVal = "";
            retVal += "Alarm Time: " + alarmTime + "\n";
            retVal += "Alarm Type: " + alarmType + "\n";
            retVal += "Priority: " + alarmPriority + "\n";
            retVal += "Tag Id: " + tagId + "\n";
            return retVal;
        }

        public void TriggerAlarm()
        {
            alarmTime = DateTime.Now;
            if (AlarmTriggered != null)
            {                        
                string alarmMessage = "| Alarm Type: " + alarmType + " | Tag ID: " + tagId + " | Alarm Time: " + alarmTime + " | Priority: " + alarmPriority;
                AlarmTriggered(alarmMessage);             
            }
        }
    }

    public enum Priority { LOW, MEDIUM, HIGH}
    public enum AlarmType { None, LoLo, Lo, Hi, HiHi, Close, Open, ChangeOfState }
}
