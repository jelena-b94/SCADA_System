﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    public class SimulationDriver
    {
        [DataMember]
        public double amplitude;
        [DataMember]
        public double period;
        [DataMember]
        public double phaseShift;
        [DataMember]
        public double verticalShift;
        [DataMember]
        public double valueS;
        [DataMember]
        public double temp;

        public double Amplitude
        {
            get { return amplitude; }
            set { amplitude = value; }
        }

        public double Period
        {
            get { return period; }
            set { period = value; }
        }

        public double PhaseShift
        {
            get { return phaseShift; }
            set { phaseShift = value; }
        }

        public double VerticalShift
        {
            get { return verticalShift; }
            set { verticalShift = value; }
        }
        public double Value
        {
            get { return valueS; }
            set { valueS = value; }
        }
        public double Temp
        {
            get { return temp; }
            set { temp = value; }
        }

        public SimulationDriver()
        {
            this.amplitude = 20;
            this.period = 120;
            this.phaseShift = 0;
            this.verticalShift = 0;
            this.valueS = 0;
        }

        public SimulationDriver(double amplitude)
        {
            this.amplitude = amplitude;
            this.period = 120;
            this.phaseShift = 0;
            this.verticalShift = 0;
            this.valueS = 0;
        }

        public SimulationDriver(double amplitude, double period, double phaseShift, double verticalShift)
        {
            this.amplitude = amplitude;
            this.period = period;
            this.phaseShift = phaseShift;
            this.verticalShift = verticalShift;
            this.valueS = 0;
        }

        public double Sine()
        {
            return verticalShift + amplitude * Math.Sin((double)DateTime.Now.Second * 2 * Math.PI / period - 2 * Math.PI * phaseShift / period);
        }

        public double Cosine()
        {
            return amplitude + amplitude * Math.Cos((double)DateTime.Now.Second * 2 * Math.PI / period - 2 * Math.PI * phaseShift / period);
        }

        public double Ramp()
        {

            //return 10 + 10*(DateTime.Now.Second / 60);
            double epsilon = 1;
            double f1 = verticalShift + amplitude * Math.Sin((double)DateTime.Now.Second * 2 * Math.PI / period - 2 * Math.PI * phaseShift / period);
            double f2 = verticalShift + amplitude * Math.Sin(((double)DateTime.Now.Second + epsilon) * 2 * Math.PI / period - 2 * Math.PI * phaseShift / period);

            return (f2 - f1) > 0 ? ((double)DateTime.Now.Second * amplitude * 4 / period + verticalShift) : ((double)DateTime.Now.Second * amplitude * 4 / period - verticalShift);
        }

        public double Rectangle()
        {
            return ((DateTime.Now.Second) % 2) == 1 ? (verticalShift + amplitude + amplitude) : (verticalShift + amplitude - amplitude);
            //return 4 / Math.PI * (Math.Sin((double)DateTime.Now.Second * 2 * Math.PI / period) + 1 / 3 * Math.Sin((double)DateTime.Now.Second * 6 * Math.PI / period) + 1 / 5 * Math.Sin((double)DateTime.Now.Second * 10 * Math.PI / period));
            //return (Sine() - 2*amplitude) >0?(verticalShift + amplitude + amplitude):(verticalShift + amplitude - amplitude);
        }

        public double Triangle()
        {
            double epsilon = 1;
            double f1 = verticalShift + amplitude * Math.Sin((double)DateTime.Now.Second * 2 * Math.PI / period - 2 * Math.PI * phaseShift / period);
            double f2 = verticalShift + amplitude * Math.Sin(((double)DateTime.Now.Second + epsilon) * 2 * Math.PI / period - 2 * Math.PI * phaseShift / period);

            return (f2 - f1) > 0 ? ((double)DateTime.Now.Second * amplitude * 4 / period + verticalShift) : (2 * amplitude - (double)DateTime.Now.Second * amplitude * 4 / period + verticalShift);
            //return 8 / Math.Pow(Math.PI, 2) * (Math.Sin((double)DateTime.Now.Second * 2 * Math.PI / period) - 1 / 9 * Math.Sin((double)DateTime.Now.Second * 6 * Math.PI / period) + 1 / 25 * Math.Sin((double)DateTime.Now.Second * 10 * Math.PI / period));
        }

        public double Digital()
        {

            return ((int)DateTime.Now.Second % 2);

        }

        public double Same()
        {
            return temp+((int)DateTime.Now.Second % 2 == 1 ? (int)DateTime.Now.Second % 2 - 0.49 : DateTime.Now.Second % 2 + 0.49);
        }
    }
}
