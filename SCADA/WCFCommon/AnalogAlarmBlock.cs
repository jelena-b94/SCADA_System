﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    public class AnalogAlarmBlock : AlarmBlock
    {
        [DataMember]
        private double lowLow;
        [DataMember]
        private double low;
        [DataMember]
        private double high;
        [DataMember]
        private double highHigh;

        public double LowLow
        {
            get { return lowLow; }
            set { lowLow = value; }
        }

        public double Low
        {
            get { return low; }
            set { low = value; }
        }

        public double High
        {
            get { return high; }
            set { high = value; }
        }

        public double HighHigh
        {
            get { return highHigh; }
            set { highHigh = value; }
        }

        public AnalogAlarmBlock() : base() { }

        public AnalogAlarmBlock(DateTime alarmTime, AlarmType alarmType, Priority alarmPriority, string tagId, 
            //double lowLow, 
            double low, 
            double high//,
            //double highHigh
            ) : base(alarmPriority, tagId)
        {
            AlarmType = AlarmType.None;
            //this.lowLow = lowLow;
            this.low = low;
            this.high = high;
            //this.highHigh = highHigh;
        }

        public AnalogAlarmBlock(Priority alarmPriority, string tagId, 
            //double lowLow, 
            double low, 
            double high//, 
            //double highHigh
            ) : base(alarmPriority, tagId)
        {
            this.AlarmType = AlarmType.None;
            //this.lowLow = lowLow;
            this.low = low;
            this.high = high;
            //this.highHigh = highHigh;
        }

        public override string ToString()
        {
            string retVal = "########## Analog Alarm Block ##########\n" + base.ToString();
            retVal += "########################################\n";
            return retVal;
        }

        public void CheckValue(double nextValue)
        {
            switch (AlarmType)
            {
                case AlarmType.Hi:
                    {
                        if (nextValue < low)
                        {
                            AlarmType = AlarmType.Lo;
                            //trigger alarm
                            TriggerAlarm();

                        }
                    }
                    break;

                case AlarmType.Lo:
                    {
                        if (nextValue > high)
                        {
                            AlarmType = AlarmType.Hi;
                            //trigger alarm
                            TriggerAlarm();
                        }
                    }
                    break;

                case AlarmType.None:
                    {
                        if (nextValue < low)
                        {
                            AlarmType = AlarmType.Lo;
                            //trigger
                            TriggerAlarm();
                        }
                        else if (nextValue > high)
                        {
                            AlarmType = AlarmType.Hi;
                            //trigger
                            TriggerAlarm();
                        }
                    }
                    break;
            }
            /*switch (AlarmType)
            {
                case AlarmType.HiHi:
                    {
                        if (nextValue >= high && nextValue <= HighHigh)
                        {
                            AlarmType = AlarmType.Hi;
                            TriggerAlarm();
                        }                     
                        break;
                    }
                case AlarmType.Hi:
                    {
                        if (nextValue <= low && nextValue >= lowLow)
                        {
                            AlarmType = AlarmType.Lo;
                            TriggerAlarm();
                        }
                        if (nextValue >= highHigh)
                        {
                            AlarmType = AlarmType.HiHi;
                            TriggerAlarm();
                        }
                        if (nextValue < high && nextValue>low)
                        {
                            AlarmType = AlarmType.None;
                            TriggerAlarm();
                        }                      
                        break;
                    }
                case AlarmType.Lo:
                    {
                        if (nextValue <= lowLow)
                        {
                            AlarmType = AlarmType.LoLo;
                            TriggerAlarm();
                        }
                        if (nextValue > low && nextValue < high)
                        {
                            AlarmType = AlarmType.None;
                            TriggerAlarm();
                        }
                        if (nextValue >= high)
                        {
                            AlarmType = AlarmType.Hi;
                            TriggerAlarm();
                        }
                        
                        break;
                    }
                case AlarmType.None:
                    {
                        if (nextValue <= low && nextValue >= lowLow)
                        {
                            AlarmType = AlarmType.Lo;
                            TriggerAlarm();
                        }
                        if (nextValue >= high && nextValue <= HighHigh)
                        {
                            AlarmType = AlarmType.Hi;
                            TriggerAlarm();
                        }
                        
                        break;
                    }
            }*/

        }
    }
}
