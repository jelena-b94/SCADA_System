﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    [KnownType(typeof(DigitalOutput))]
    [KnownType(typeof(AnalogOutput))]
    public abstract class Output : Tag
    {

        [DataMember]
        public double initialValue { get; set; }

        public Output() { }

        public Output(string tagName, string description, string IOaddress, double initialValue)
            : base(tagName, description, IOaddress)
        {
            this.initialValue = initialValue;
        }
    }
}
