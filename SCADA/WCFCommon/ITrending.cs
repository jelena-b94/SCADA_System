﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace WCFCommon
{
    [ServiceContract]
    public interface ITrending
    {
        [OperationContract]
        Dictionary<string, Tag> displayTrending();
    }
}
