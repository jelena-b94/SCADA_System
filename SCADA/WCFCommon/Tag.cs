﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    [KnownType(typeof(Digital))]
    [KnownType(typeof(Analog))]
    public abstract class Tag
    {
        [DataMember]
        private string tagId;
        [DataMember]
        private string description;
        [DataMember]
        private SimulationDriver driver;
        [DataMember]
        private string IOaddress;

        public string TagId {
            get { return tagId; }
            set { tagId = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public SimulationDriver Driver
        {
            get { return driver; }
            set { driver = value; }
        }

        public string IOAddress
        {
            get { return IOaddress; }
            set { IOaddress = value; }
        }

        public Tag() {
            this.tagId = "";
            this.description = "";
            this.driver = new SimulationDriver();
            this.IOaddress = "";
        }

        public Tag(string tagId, string description, string IOaddress)
        {
            this.tagId = tagId;
            this.description = description;
            this.driver = new SimulationDriver();
            this.IOaddress = IOaddress;
        }

        public Tag(string tagId, string description, SimulationDriver driver, string IOaddress)
        {
            this.tagId = tagId;
            this.description = description;
            this.driver = driver;
            this.IOaddress = IOaddress;
        }

        public override string ToString()
        {
            string retVal = "\n";
            retVal += "Tag name: " + this.tagId + "\n";
            retVal += "Description: " + this.description + "\n";
            retVal += "Simulation Driver: " + this.driver + "\n";
            retVal += "I/O Address: " + this.IOaddress + "\n";
            return retVal;
        }

    }
}
