﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    [KnownType(typeof(DigitalInput))]
    [KnownType(typeof(AnalogInput))]
    public abstract class Input : Tag
    {
        [DataMember]
        public int scanTime { get; set; }

        [DataMember]
        public AlarmBlock alarm { get; set; }

        [DataMember]
        public bool onOffScan { get; set; }

        [DataMember]
        public bool autoManual { get; set; }

        public Input() { }

        public Input(string tagName, string description, string IOaddress, int scanTime, AlarmBlock alarm = null, bool onOffScan = true, bool autoManual = true)
            : base(tagName, description, IOaddress)
        {
            this.scanTime = scanTime;
            this.alarm = alarm;
            this.onOffScan = onOffScan;
            this.autoManual = autoManual;
        }
    }
}
