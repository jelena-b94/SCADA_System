﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    [KnownType(typeof(AnalogInput))]
    [KnownType(typeof(AnalogOutput))]
    public abstract class Analog : Tag
    {
        [DataMember]
        private double lowLimit;
        [DataMember]
        private double highLimit;
        [DataMember]
        private string units;

        public double LowLimit
        {
            get { return lowLimit; }
            set { lowLimit = value; }
        }

        public double HighLimit
        {
            get { return highLimit; }
            set { highLimit = value; }
        }

        public string Units
        {
            get { return units; }
            set { units = value; }
        }

        public Analog() : base()
        {
            this.lowLimit = 0;
            this.highLimit = 100000;
            this.units = "";
        }
        public Analog(string tagId, string description, string IOaddress, double lowLimit, double highLimit, string units)
            : base(tagId, description, IOaddress)
        {
            this.lowLimit = lowLimit;
            this.highLimit = highLimit;
            this.units = units;
        }

        public override string ToString()
        {
            string retVal = base.ToString();
            retVal += "Low Limit: " + this.lowLimit + "\n";
            retVal += "High Limit: " + this.highLimit + "\n";
            retVal += "Units: " + this.units + "\n";
            return retVal;
        }
    }
}
