﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    public class DigitalAlarmBlock : AlarmBlock
    {
        public DigitalAlarmBlock() : base() { }

        public DigitalAlarmBlock(DateTime alarmTime, AlarmType alarmType, Priority alarmPriority, string tagId) : base(alarmPriority, tagId)
        {
        }

        public DigitalAlarmBlock(Priority alarmPriority, string tagId) : base(alarmPriority, tagId)
        {
            this.AlarmType = AlarmType.None;
        }
        public DigitalAlarmBlock(string tagId, Priority alarmPriority, AlarmType type) : base(alarmPriority, tagId)
        {
            this.AlarmType = type;
        }

        public override string ToString()
        {
            string retVal = "########## Digital Alarm Block ##########\n" + base.ToString();
            retVal += "#########################################\n";
            return retVal;
        }

        public void CheckValue(double previousValue, double nextValue)
        {
            switch (AlarmType)
            {
                case AlarmType.Open:
                    {
                        if (nextValue == 1)
                            TriggerAlarm();
                        break;
                    }
                case AlarmType.Close:
                    {
                        if (nextValue == 1)
                            TriggerAlarm();
                        break;
                    }
                case AlarmType.ChangeOfState:
                    {
                        if (previousValue != nextValue)
                            TriggerAlarm();
                        break;
                    }
            }
            

        }
    }
}
