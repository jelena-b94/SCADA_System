﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WCFCommon
{
    public class AlarmFileLogger
    {
        FileStream fs;
        StreamWriter sw;

        public AlarmFileLogger(string fileName)
        {
            fs = new FileStream(fileName, FileMode.Append, FileAccess.Write);
            sw = new StreamWriter(fs);
        }

        public void Logger(string message)
        {
            sw.WriteLine(message);
        }

        public void Close()
        {
            sw.Close();
            fs.Close();
        }
    }
}
